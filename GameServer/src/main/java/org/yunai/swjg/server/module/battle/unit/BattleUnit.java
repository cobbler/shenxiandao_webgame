package org.yunai.swjg.server.module.battle.unit;

import org.yunai.swjg.server.core.role.AbstractRole;

/**
 * 战斗单元
 * User: yunai
 * Date: 13-5-23
 * Time: 下午4:39
 */
public class BattleUnit {

    // ==================== 战斗单元最初数值属性 ====================
    /**
     * 最大血量
     */
    private int baseHpMax;

    // ==================== 战斗中计算后战斗数值属性 ====================
    /**
     * 当前血量
     */
    private int hpCur;
    /**
     * 最大血量
     */
    private int hpMax;

    public static BattleUnit getInstance(AbstractRole role, boolean isAttack, boolean isFullHp) {
        BattleUnit unit = new BattleUnit();
        unit.hpMax = role.getHpMax();
        if (isFullHp) {
            unit.hpCur = unit.hpMax;
        } else {
            unit.hpCur = role.getHpCur();
        }
        return unit;
    }

    public int getBaseHpMax() {
        return baseHpMax;
    }

    public void setBaseHpMax(int baseHpMax) {
        this.baseHpMax = baseHpMax;
    }

    public int getHpCur() {
        return hpCur;
    }

    public void setHpCur(int hpCur) {
        this.hpCur = hpCur;
    }

    public int getHpMax() {
        return hpMax;
    }

    public void setHpMax(int hpMax) {
        this.hpMax = hpMax;
    }
}
