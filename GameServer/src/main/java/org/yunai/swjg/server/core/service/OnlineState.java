package org.yunai.swjg.server.core.service;

import org.yunai.yfserver.util.CollectionUtils;

import java.util.Set;

/**
 * 在线状态枚举<br />
 * 玩家在线状态统一由主线程维护
 * User: yunai
 * Date: 13-4-2
 * Time: 下午1:00
 */
public enum OnlineState {

    /**
     * 未连接
     */
    unconnected,
    /**
     * 已连接
     */
    connected,
    /**
     * 已认证(特殊)<br />
     * 当当前用户把另外的用户顶下去的时候使用
     */
    temp_authed,
    /**
     * 已认证
     */
    authed,
    /**
     * 客户端创建角色中
     */
    client_create_roleing,
    /**
     * 客户端已创建角色
     */
    client_create_roled,
    /**
     * 正在加载角色
     */
    load_roleing,
    /**
     * 角色加载完成
     */
    load_roled,
    /**
     * 正在进入游戏场景
     */
    enter_sceneing,
    /**
     * 正在游戏
     */
    gaming,
    /**
     * 正在退出场景
     */
//    leave_sceneing,
    /**
     * 正在退出游戏
     */
    logouting,
    /**
     * 已游戏游戏
     */
    logouted;

    /**
     * 前置状态<br />
     * 切换状态时，当且仅当前置状态时候，才可以进行转换
     */
    private Set<OnlineState> preStateSet;

    static {
        unconnected.preStateSet = CollectionUtils.emptySet(OnlineState.class);
        connected.preStateSet = CollectionUtils.asSet(unconnected);
        temp_authed.preStateSet = CollectionUtils.asSet(connected);
        authed.preStateSet = CollectionUtils.asSet(connected, temp_authed);
        client_create_roleing.preStateSet = CollectionUtils.asSet(authed);
        client_create_roled.preStateSet = CollectionUtils.asSet(client_create_roleing);
        load_roleing.preStateSet = CollectionUtils.asSet(authed, client_create_roled);
        load_roled.preStateSet = CollectionUtils.asSet(load_roleing);
        enter_sceneing.preStateSet = CollectionUtils.asSet(load_roled);
        gaming.preStateSet = CollectionUtils.asSet(enter_sceneing);
//        leave_sceneing.preStateSet = CollectionUtils.asSet(gaming);
        logouting.preStateSet = CollectionUtils.asSet(connected, temp_authed, authed, client_create_roleing,
                client_create_roled, load_roleing, load_roled, enter_sceneing, gaming /**, leave_sceneing */);
        logouted.preStateSet = CollectionUtils.asSet(logouting);
    }

    /**
     * 切换到目标状态，并返回切换是否成功
     *
     * @param targetState 目标状态
     * @return 切换是否成功
     */
    public boolean canChange(OnlineState targetState) {
        return targetState != null && targetState.preStateSet.contains(this);
    }

//    @Deprecated
    // 流程路线
    // connected => authed => [client_create_roleing => client_create_roled] => load_roleing => load_roled => gaming => logouting => logouted
    //                        [此处流程在无角色时候产生                  ]
}
