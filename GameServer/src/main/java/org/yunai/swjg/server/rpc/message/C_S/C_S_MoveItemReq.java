package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20806】: 背包道具移动请求
 */
public class C_S_MoveItemReq extends GameMessage {
    public static final short CODE = 20806;

    /**
     * 道具背包编号
     */
    private Byte fromBagId;
    /**
     * 道具背包位置
     */
    private Byte fromBagIndex;
    /**
     * 道具背包携带者
     */
    private Integer fromWearId;
    /**
     * 目标背包编号
     */
    private Byte toBagId;
    /**
     * 目标背包位置
     */
    private Byte toBagIndex;
    /**
     * 目标背包携带者
     */
    private Integer toWearId;

    public C_S_MoveItemReq() {
    }

    public C_S_MoveItemReq(Byte fromBagId, Byte fromBagIndex, Integer fromWearId, Byte toBagId, Byte toBagIndex, Integer toWearId) {
        this.fromBagId = fromBagId;
        this.fromBagIndex = fromBagIndex;
        this.fromWearId = fromWearId;
        this.toBagId = toBagId;
        this.toBagIndex = toBagIndex;
        this.toWearId = toWearId;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getFromBagId() {
		return fromBagId;
	}

	public void setFromBagId(Byte fromBagId) {
		this.fromBagId = fromBagId;
	}
	public Byte getFromBagIndex() {
		return fromBagIndex;
	}

	public void setFromBagIndex(Byte fromBagIndex) {
		this.fromBagIndex = fromBagIndex;
	}
	public Integer getFromWearId() {
		return fromWearId;
	}

	public void setFromWearId(Integer fromWearId) {
		this.fromWearId = fromWearId;
	}
	public Byte getToBagId() {
		return toBagId;
	}

	public void setToBagId(Byte toBagId) {
		this.toBagId = toBagId;
	}
	public Byte getToBagIndex() {
		return toBagIndex;
	}

	public void setToBagIndex(Byte toBagIndex) {
		this.toBagIndex = toBagIndex;
	}
	public Integer getToWearId() {
		return toWearId;
	}

	public void setToWearId(Integer toWearId) {
		this.toWearId = toWearId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_MoveItemReq struct = new C_S_MoveItemReq();
            struct.setFromBagId(byteArray.getByte());
            struct.setFromBagIndex(byteArray.getByte());
            struct.setFromWearId(byteArray.getInt());
            struct.setToBagId(byteArray.getByte());
            struct.setToBagIndex(byteArray.getByte());
            struct.setToWearId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_MoveItemReq struct = (C_S_MoveItemReq) message;
            ByteArray byteArray = ByteArray.createNull(12);
            byteArray.create();
            byteArray.putByte(struct.getFromBagId());
            byteArray.putByte(struct.getFromBagIndex());
            byteArray.putInt(struct.getFromWearId());
            byteArray.putByte(struct.getToBagId());
            byteArray.putByte(struct.getToBagIndex());
            byteArray.putInt(struct.getToWearId());
            return byteArray;
        }
    }
}