package org.yunai.swjg.server.module.quest;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 任务定义接口，该类不会进行实现的
 * User: yunai
 * Date: 13-5-8
 * Time: 下午11:48
 */
public interface QuestDef {

    /**
     * 任务类型枚举
     */
    public static enum Type implements IndexedEnum {
        /**
         * 空类型
         */
        NULL(0),
        /**
         * 主线任务
         */
        MAIN(1),
        /**
         * 支线任务
         */
        BRANCH(2),
        /**
         * 日常任务
         */
        DAILY(3),
        /**
         * 周长任务
         */
        WEEKLY(4),
        /**
         * 帮会任务
         */
        GANG(5),
        /**
         * 国家任务
         */
        COUNTRY(6);

        /**
         * 分类编号
         */
        private final int index;

        private Type(int index) {
            this.index = index;
        }

        @Override
        public int getIndex() {
            return index;
        }

        private static final List<Type> VALUES = IndexedEnum.Util.toIndexes(values());

        public static Type valueOf(Integer index) {
            return Util.valueOf(VALUES, index);
        }
    }

    /**
     * 任务条件枚举
     */
    public static enum Condition implements IndexedEnum {
        /**
         * 给予道具条件
         */
        ITEM(1),
        /**
         * 杀怪任务条件
         */
        KILL(2);

        /**
         * 任务条件编号
         */
        private final int index;

        private Condition(int index) {
            this.index = index;
        }

        @Override
        public int getIndex() {
            return index;
        }

        /**
         * 将index转为byte返回
         *
         * @return index的byte范围的值
         */
        public byte getValue() {
            return (byte) index;
        }

        private static final List<Condition> VALUES = IndexedEnum.Util.toIndexes(values());

        public static Condition valueOf(Integer index) {
            return Util.valueOf(VALUES, index);
        }
    }

    /**
     * 任务状态枚举
     */
    public static enum Status implements IndexedEnum {
        /**
         * 接受状态
         */
        ACCEPTED(1),
        /**
         * 可以完成状态
         */
        CAN_FINISHED(2),
        /**
         * 可以接受状态
         */
        CAN_ACCEPTED(3);

        /**
         * 任务状态编号
         */
        private final int index;

        private Status(int index) {
            this.index = index;
        }

        @Override
        public int getIndex() {
            return index;
        }

        /**
         * 将index转为byte返回
         *
         * @return index的byte范围的值
         */
        public byte getValue() {
            return (byte) index;
        }

        private static final List<Status> VALUES = IndexedEnum.Util.toIndexes(values());

        public static Status valueOf(Integer index) {
            return Util.valueOf(VALUES, index);
        }
    }
}
