package org.yunai.swjg.server.module.scene.msg;

import org.yunai.swjg.server.core.annotation.SceneThread;
import org.yunai.swjg.server.core.service.GameMessageProcessor;
import org.yunai.swjg.server.module.scene.NormalSceneService;
import org.yunai.swjg.server.module.scene.core.msg.SysPlayerEnterSceneResult;
import org.yunai.yfserver.message.sys.SysInternalMessage;
import org.yunai.yfserver.server.IMessageProcessor;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家进入普通场景<br />
 * 系统内部调用消息
 * User: yunai
 * Date: 13-4-23
 * Time: 下午2:18
 */
public class SysPlayerEnterNormalScene
        extends SysInternalMessage {

    private static NormalSceneService sceneService;
    private static IMessageProcessor messageProcessor;
    static {
        sceneService = BeanManager.getBean(NormalSceneService.class);
        messageProcessor = BeanManager.getBean(GameMessageProcessor.class);
    }

    /**
     * 玩家编号
     */
    private final Integer playerId;
    /**
     * 场景编号
     */
    private final Integer sceneId;
    /**
     * 场景分线
     */
    private final Integer line;

    public SysPlayerEnterNormalScene(Integer playerId, Integer sceneId, Integer line) {
        this.playerId = playerId;
        this.sceneId = sceneId;
        this.line = line;
    }

    /**
     * [场景线程]处理玩家进入场景，之后给[主消息队列]发送进入场景结果
     */
    @Override
    @SceneThread
    public void execute() {
        boolean success = sceneService.handleEnterNormalScene(playerId, sceneId, line);
        SysPlayerEnterSceneResult playerEnterSceneResult = new SysPlayerEnterSceneResult(playerId, success);
        messageProcessor.put(playerEnterSceneResult);
    }

    @Override
    public short getCode() {
        return 0;
    }
}
