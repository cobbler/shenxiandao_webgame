package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 竞技场排行数据实体
 * User: yunai
 * Date: 13-5-26
 * Time: 上午2:14
 */
public class ArenaRankEntity implements Entity<Integer> {

    /**
     * 玩家编号
     */
    private int id;
    /**
     * 上次重置时间
     */
    private long lastResetTime;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 等级
     */
    private short level;
    /**
     * 职业
     */
    private short vocation;
    /**
     * 性别
     */
    private short sex;
    /**
     * 剩余挑战次数
     */
    private int challenge;
    /**
     * 挑战购买次数
     */
    private int challengeBuy;
    /**
     * 再次可可以挑战时间，单位（毫秒）。<br />
     * 0代表无CD
     */
    private long challengeCD;
    /**
     * 连胜次数
     */
    private int winStreak;
    /**
     * 排行
     */
    private int rank;
    /**
     * 上上次排行
     */
    private int lastLastRank;
    /**
     * 上次排行
     */
    private int lastRank;
    /**
     * 角色队伍战力
     */
    private int power;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public long getLastResetTime() {
        return lastResetTime;
    }

    public void setLastResetTime(long lastResetTime) {
        this.lastResetTime = lastResetTime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public short getVocation() {
        return vocation;
    }

    public void setVocation(short vocation) {
        this.vocation = vocation;
    }

    public short getSex() {
        return sex;
    }

    public void setSex(short sex) {
        this.sex = sex;
    }

    public int getChallenge() {
        return challenge;
    }

    public void setChallenge(int challenge) {
        this.challenge = challenge;
    }

    public int getChallengeBuy() {
        return challengeBuy;
    }

    public void setChallengeBuy(int challengeBuy) {
        this.challengeBuy = challengeBuy;
    }

    public long getChallengeCD() {
        return challengeCD;
    }

    public void setChallengeCD(long challengeCD) {
        this.challengeCD = challengeCD;
    }

    public int getWinStreak() {
        return winStreak;
    }

    public void setWinStreak(int winStreak) {
        this.winStreak = winStreak;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getLastLastRank() {
        return lastLastRank;
    }

    public void setLastLastRank(int lastLastRank) {
        this.lastLastRank = lastLastRank;
    }

    public int getLastRank() {
        return lastRank;
    }

    public void setLastRank(int lastRank) {
        this.lastRank = lastRank;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}