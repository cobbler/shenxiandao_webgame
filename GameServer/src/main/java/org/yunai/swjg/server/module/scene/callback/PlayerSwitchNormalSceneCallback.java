package org.yunai.swjg.server.module.scene.callback;

import org.yunai.swjg.server.core.annotation.MainThread;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.module.scene.NormalSceneService;
import org.yunai.swjg.server.module.scene.core.SceneCallable;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家换场景的场景回调
 * User: yunai
 * Date: 13-4-26
 * Time: 下午2:36
 */
public class PlayerSwitchNormalSceneCallback implements SceneCallable {

    private static OnlineContextService onlineContextService;
    private static NormalSceneService sceneService;
    static {
        onlineContextService = BeanManager.getBean(OnlineContextService.class);
        sceneService = BeanManager.getBean(NormalSceneService.class);
    }

    /**
     * 在线信息
     */
    private final Integer playerId;
    /**
     * 切换场景编号
     */
    private final Integer sceneId;
    /**
     * 切换场景坐标X
     */
    private final Short sceneX;
    /**
     * 切换场景坐标Y
     */
    private final Short sceneY;

    public PlayerSwitchNormalSceneCallback(Integer playerId, Integer sceneId, Short sceneX, Short sceneY) {
        this.playerId = playerId;
        this.sceneId = sceneId;
        this.sceneX = sceneX;
        this.sceneY = sceneY;
    }

    @Override
    @MainThread
    public void call() {
        Online online = onlineContextService.getPlayer(playerId);
        if (online == null) {
            return;
        }
        sceneService.onPlayerEnterNormalScene(online, sceneId, sceneX, sceneY);
    }
}
