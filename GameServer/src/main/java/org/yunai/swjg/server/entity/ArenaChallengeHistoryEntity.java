package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 竞技场挑战历史<br />
 * 玩家每次挑战之后，会插入两条记录。<br />
 * 一条自己的，一条对手玩家的.
 * User: yunai
 * Date: 13-5-26
 * Time: 上午1:47
 */
public class ArenaChallengeHistoryEntity implements Entity<Integer> {

    /**
     * ID，主键自增
     */
    private int id;
    /**
     * 玩家编号
     */
    private int pid;
    /**
     * 对手编号
     */
    private int opponentPid;
    /**
     * 对手昵称
     */
    private String opponentNickname;
    /**
     * 挑战时间
     */
    private int time;
    /**
     * 挑战前排名
     */
    private int rank;
    /**
     * 挑战后排名
     */
    private int newRank;
    /**
     * 是否胜利
     */
    private boolean win;
    /**
     * 是否主动攻击
     */
    private boolean active;
    /**
     * 战报编号
     */
    private int battleId;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getOpponentPid() {
        return opponentPid;
    }

    public void setOpponentPid(int opponentPid) {
        this.opponentPid = opponentPid;
    }

    public String getOpponentNickname() {
        return opponentNickname;
    }

    public void setOpponentNickname(String opponentNickname) {
        this.opponentNickname = opponentNickname;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getNewRank() {
        return newRank;
    }

    public void setNewRank(int newRank) {
        this.newRank = newRank;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getBattleId() {
        return battleId;
    }

    public void setBattleId(int battleId) {
        this.battleId = battleId;
    }
}