package org.yunai.swjg.server.core.message;

import org.yunai.swjg.server.core.session.GameSession;
import org.yunai.yfserver.plugin.mina.message.AbstractMinaMessage;

/**
 * 游戏消息基类
 * User: yunai
 * Date: 13-3-26
 * Time: 下午8:14
 */
public abstract class GameMessage extends AbstractMinaMessage<GameSession> {
}
