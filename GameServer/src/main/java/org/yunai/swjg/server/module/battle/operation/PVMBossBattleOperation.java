package org.yunai.swjg.server.module.battle.operation;

import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.activity.BossActivity;
import org.yunai.swjg.server.module.battle.unit.BattleUnit;
import org.yunai.swjg.server.module.monster.vo.VisibleMonster;

/**
 * 玩家与BOSS副本怪物战斗操作
 * User: yunai
 * Date: 13-5-20
 * Time: 下午8:05
 */
public class PVMBossBattleOperation extends PVMBattleOperation {

    /**
     * 战斗前血量
     */
    private int oldHp = 0;
    /**
     * BOSS活动对象
     */
    private final BossActivity bossActivity;

    public PVMBossBattleOperation(Online online, VisibleMonster monster, BossActivity bossActivity) {
        // 初始化
        super(online, monster, true, false);
        // 赋值
        this.bossActivity = bossActivity;
        // 计算原来血量
        for (BattleUnit unit : defenseTeam.getUnits()) {
            oldHp += unit.getHpCur();
        }
    }

    @Override
    protected void pvmEndImpl(boolean attWin) {
        // 修改血量
        int nowHp = 0;
        for (BattleUnit unit : defenseTeam.getUnits()) {
            nowHp += unit.getHpCur();
        }
        if (nowHp < 0) {
            nowHp = 0;
        }
        // 战斗结算
        bossActivity.playerEndAttack(online, monster, oldHp, nowHp);
    }

}