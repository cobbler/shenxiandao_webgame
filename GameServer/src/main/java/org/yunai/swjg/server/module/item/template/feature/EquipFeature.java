package org.yunai.swjg.server.module.item.template.feature;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 装备实例属性接口
 * User: yunai
 * Date: 13-4-7
 * Time: 下午2:48
 */
public class EquipFeature implements ItemFeature {

    /**
     * 等级
     */
    @JSONField(name = "l")
    private short level;

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }
}
