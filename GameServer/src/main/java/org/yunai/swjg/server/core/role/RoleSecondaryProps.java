package org.yunai.swjg.server.core.role;

import org.yunai.yfserver.annotation.Type;
import org.yunai.yfserver.object.KeyValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色二级属性(计算出来的属性)
 * User: yunai
 * Date: 13-5-2
 * Time: 下午10:04
 */
public class RoleSecondaryProps extends PropertyObject {

    private static final int BEGIN = 0;
    private static int END = BEGIN;

    /**
     * 武力
     */
    @Type(Integer.class)
    public static final int STRENGTH = END++;
    /**
     * 绝技
     */
    @Type(Integer.class)
    public static final int STUNT = END++;
    /**
     * 法术
     */
    @Type(Integer.class)
    public static final int MAGIC = END++;
    /**
     * 普通攻击
     */
    @Type(Integer.class)
    public static final int STRENGTH_ATTACK = END++;
    /**
     * 绝技攻击
     */
    @Type(Integer.class)
    public static final int STUNT_ATTACK = END++;
    /**
     * 法术攻击
     */
    @Type(Integer.class)
    public static final int MAGIC_ATTACK = END++;
    /**
     * 普通防御
     */
    @Type(Integer.class)
    public static final int STRENGTH_DEFENSE = END++;
    /**
     * 绝技防御
     */
    @Type(Integer.class)
    public static final int STUNT_DEFENSE = END++;
    /**
     * 法术防御
     */
    @Type(Integer.class)
    public static final int MAGIC_DEFENSE = END++;
    /**
     * 血上限
     */
    @Type(Double.class)
    public static final int HP_MAX = END++;
    /**
     * 当前血量
     */
    @Type(Double.class)
    public static final int HP_CUR = END++;
    /**
     * 暴击，万分比
     */
    @Type(Integer.class)
    public static final int CRIT = END++;
    /**
     * 命中，万分比
     */
    @Type(Integer.class)
    public static final int HIT = END++;
    /**
     * 破击，万分比
     */
    @Type(Integer.class)
    public static final int POJI = END++;
    /**
     * 必杀，万分比
     */
    @Type(Integer.class)
    public static final int BISHA = END++;
    /**
     * 韧性，万分比
     */
    @Type(Integer.class)
    public static final int TOUGHNESS = END++;
    /**
     * 闪避，万分比
     */
    @Type(Integer.class)
    public static final int DODGE = END++;
    /**
     * 格挡，万分比
     */
    @Type(Integer.class)
    public static final int BLOCK = END++;
    /**
     * 先攻
     */
    @Type(Integer.class)
    public static final int XIANGONG = END++;
    /**
     * 战力
     */
    @Type(Integer.class)
    public static final int POWER = END++;
    /**
     * 命力
     */
    @Type(Integer.class)
    public static final int MINGLI = END++;

    /**
     * 属性个数
     */
    public static final int SIZE = END;
    /**
     * 属性类型
     */
    public static final RoleDef.PropertyType TYPE = RoleDef.PropertyType.ROLE_PROPS_SECONDARY;

    public RoleSecondaryProps() {
        super(SIZE, TYPE);
    }

    public List<KeyValuePair<Integer, Integer>> getChangedNum() {
        return this.convertKeyValuePair(super.getChanged());
    }

    public List<KeyValuePair<Integer, Integer>> getAllNum() {
        return this.convertKeyValuePair(super.getIndexValuePairs());
    }

//    /**
//     * 将double类型的数据全部转化成int类型<br />
//     * CRIT/DODGE/HIT/ATTACK_SPEED属性先乘以SharedConstants.DOUBLE_FACTOR。
//     * 之后所有属性四舍五入成整数
//     *
//     * @param pairs 键值对
//     * @return 转换后的数组
//     */
    private List<KeyValuePair<Integer, Integer>> convertKeyValuePair(KeyValuePair<Integer, Object>[] pairs) {
        List<KeyValuePair<Integer, Integer>> result = new ArrayList<>(pairs.length);
        for (KeyValuePair<Integer, Object> pair : pairs) {
//            int index = super.getIndex(pair.getKey());
//            Double val = (Double) pair.getValue();
//            if (index == CRIT || index == DODGE || index == HIT || index == ATTACK_SPEED) {
//                val *= SharedConstants.DOUBLE_FACTOR;
//            }
//            result.give(new KeyValuePair<>(pair.getKey(), (int) Math.round(val)));
            result.add(new KeyValuePair<>(pair.getKey(), (Integer) pair.getValue()));
        }
        return result;
    }
}
