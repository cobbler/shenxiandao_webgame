package org.yunai.swjg.server.module.item.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.item.ItemService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_DropItemReq;

import javax.annotation.Resource;

/**
 * 道具丢弃命令
 * User: yunai
 * Date: 13-4-12
 * Time: 上午10:20
 */
@Component
public class DropItemCommand extends GameMessageCommand<C_S_DropItemReq> {

    @Resource
    private ItemService itemService;

    @Override
    public void execute(Online online, C_S_DropItemReq msg) {
        itemService.dropItem(online, msg.getBagId().byteValue(), msg.getBagIndex().intValue());
    }
}