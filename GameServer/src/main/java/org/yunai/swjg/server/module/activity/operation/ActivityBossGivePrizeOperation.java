package org.yunai.swjg.server.module.activity.operation;

import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.core.thread.AbstractOtherPlayerOperation;
import org.yunai.swjg.server.module.currency.Currency;
import org.yunai.swjg.server.module.currency.CurrencyService;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 活动（BOSS活动本）给予玩家奖励操作
 * User: yunai
 * Date: 13-5-22
 * Time: 上午9:01
 */
public class ActivityBossGivePrizeOperation extends AbstractOtherPlayerOperation {

    private static CurrencyService currencyService;
    static {
        currencyService = BeanManager.getBean(CurrencyService.class);
    }

    public ActivityBossGivePrizeOperation(Integer playerId) {
        super(playerId);
    }

    @Override
    protected void doOnlineImpl(Online online) {
        if (currencyService.canGive(online.getPlayer(), Currency.COIN, 10000, false)) {
            currencyService.give(online.getPlayer(), Currency.COIN, 10000);
        }
    }

    @Override
    protected void doOfflineImpl() {
        Online online = Online.loadOffline(getPlayerId());
        if (currencyService.canGive(online.getPlayer(), Currency.COIN, 10000, false)) {
            currencyService.give(online.getPlayer(), Currency.COIN, 10000);
        }
        online.updateData();
    }

}
