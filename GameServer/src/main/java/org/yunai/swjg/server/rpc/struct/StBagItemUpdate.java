package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 背包道具更新结构体
 */
public class StBagItemUpdate implements IStruct {
    /**
     * 背包编号
     */
    private Byte bagId;
    /**
     * 背包位置
     */
    private Byte bagIndex;
    /**
     * 道具编号
     */
    private Integer id;
    /**
     * 堆叠数量
     */
    private Integer overlap;
    /**
     * 模板编号
     */
    private Integer templateId;
    /**
     * 携带着编号。目前有玩家和伙伴
     */
    private Integer wearId;

    public StBagItemUpdate() {
    }

    public StBagItemUpdate(Byte bagId, Byte bagIndex, Integer id, Integer overlap, Integer templateId, Integer wearId) {
        this.bagId = bagId;
        this.bagIndex = bagIndex;
        this.id = id;
        this.overlap = overlap;
        this.templateId = templateId;
        this.wearId = wearId;
    }

	public Byte getBagId() {
		return bagId;
	}

	public void setBagId(Byte bagId) {
		this.bagId = bagId;
	}
	public Byte getBagIndex() {
		return bagIndex;
	}

	public void setBagIndex(Byte bagIndex) {
		this.bagIndex = bagIndex;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOverlap() {
		return overlap;
	}

	public void setOverlap(Integer overlap) {
		this.overlap = overlap;
	}
	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public Integer getWearId() {
		return wearId;
	}

	public void setWearId(Integer wearId) {
		this.wearId = wearId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StBagItemUpdate struct = new StBagItemUpdate();
            struct.setBagId(byteArray.getByte());
            struct.setBagIndex(byteArray.getByte());
            struct.setId(byteArray.getInt());
            struct.setOverlap(byteArray.getInt());
            struct.setTemplateId(byteArray.getInt());
            struct.setWearId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StBagItemUpdate struct = (StBagItemUpdate) message;
            ByteArray byteArray = ByteArray.createNull(18);
            byteArray.create();
            byteArray.putByte(struct.getBagId());
            byteArray.putByte(struct.getBagIndex());
            byteArray.putInt(struct.getId());
            byteArray.putInt(struct.getOverlap());
            byteArray.putInt(struct.getTemplateId());
            byteArray.putInt(struct.getWearId());
            return byteArray;
        }
    }
}