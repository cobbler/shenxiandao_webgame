package org.yunai.swjg.server.core.role;

import org.yunai.yfserver.annotation.Type;
import org.yunai.yfserver.object.KeyValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色一级属性(非计算属性)
 * User: yunai
 * Date: 13-5-2
 * Time: 下午10:04
 */
public class RolePrimaryProps extends PropertyObject {

    private static final int BEGIN = 0;
    private static int END = BEGIN;

    /**
     * 职业
     */
    @Type(Short.class)
    public static final int VOCATION = END++;
    /**
     * 等级
     */
    @Type(Short.class)
    public static final int LEVEL = END++;
    /**
     * 性别
     */
    @Type(Byte.class)
    public static final int SEX = END++;
    /**
     * 经验
     */
    @Type(Integer.class)
    public static final int EXP = END++;
    /**
     * 场景编号
     */
    @Type(Integer.class)
    public static final int SCENE_ID = END++;
    /**
     * 坐标X
     */
    @Type(Short.class)
    public static final int SCENE_X = END++;
    /**
     * 坐标Y
     */
    @Type(Short.class)
    public static final int SCENE_Y = END++;
    /**
     * 上一个场景编号.<br />
     * 该属性的主要作用是，从副本回到原来场景时，进行读取。<br />
     * 当玩家在副本里下线时，将会退回到上一个场景。并且{@link #SCENE_PRE_ID}、{@link #SCENE_PRE_X}、{@link #SCENE_PRE_Y}全部被设置为0
     */
    @Type(Integer.class)
    public static final int SCENE_PRE_ID = END++;
    /**
     * 上一个场景坐标X
     * @see #SCENE_PRE_ID
     */
    @Type(Integer.class)
    public static final int SCENE_PRE_X = END++;
    /**
     * 上一个场景坐标Y
     * @see #SCENE_PRE_ID
     */
    @Type(Integer.class)
    public static final int SCENE_PRE_Y = END++;
    /**
     * 游戏币
     */
    @Type(Integer.class)
    public static final int COIN = END++;
    /**
     * 元宝
     */
    @Type(Integer.class)
    public static final int GOLD = END++;
    /**
     * 声望
     */
    @Type(Integer.class)
    public static final int REPUTATION = END++;
    /**
     * 阅历
     */
    @Type(Integer.class)
    public static final int SOUL = END++;
    /**
     * 培养的武力
     */
    @Type(Integer.class)
    public static final int DEVELOP_STRENGTH = END++;
    /**
     * 培养的绝技
     */
    @Type(Integer.class)
    public static final int DEVELOP_STUNT = END++;
    /**
     * 培养的法术
     */
    @Type(Integer.class)
    public static final int DEVELOP_MAGIC = END++;

    /**
     * 属性个数
     */
    public static final int SIZE = END;
    /**
     * 属性类型
     */
    public static final RoleDef.PropertyType TYPE = RoleDef.PropertyType.ROLE_PROPS_PRIMARY;

    public RolePrimaryProps() {
        super(SIZE, TYPE);
    }

    public List<KeyValuePair<Integer, Integer>> getChangedNum() {
        return this.convertKeyValuePair(super.getChanged());
    }

    public List<KeyValuePair<Integer, Integer>> getAllNum() {
        return this.convertKeyValuePair(super.getIndexValuePairs());
    }

    private List<KeyValuePair<Integer, Integer>> convertKeyValuePair(KeyValuePair<Integer, Object>[] pairs) {
        List<KeyValuePair<Integer, Integer>> result = new ArrayList<>(pairs.length);
        for (KeyValuePair<Integer, Object> pair : pairs) {
            Object val = pair.getValue();
            if (val instanceof Byte) {
                result.add(new KeyValuePair<>(pair.getKey(), ((Byte) val).intValue()));
            } else if (val instanceof Short) {
                result.add(new KeyValuePair<>(pair.getKey(), ((Short) val).intValue()));
            } else {
                result.add(new KeyValuePair<>(pair.getKey(), (Integer) val));
            }
        }
        return result;
    }
}
