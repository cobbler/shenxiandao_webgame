package org.yunai.swjg.server.core.role;

import org.yunai.yfserver.annotation.Type;

/**
 * 角色字符串属性
 * User: yunai
 * Date: 13-5-2
 * Time: 下午10:54
 */
public class RoleStringProps extends PropertyObject {

    private static final int BEGIN = 0;
    private static int END = BEGIN;

    /**
     * 名字
     */
    @Type(String.class)
    public static final int NAME = END++;
    /**
     * 武力丹药<br />
     * 格式: a1-b1;a2-b2;a3-b3。<br />
     * 其中a1、a2等，代表丹药等级。b1、b2等，代表丹药数量。
     */
    @Type(String.class)
    public static final int MEDICINE_STRENGTH = END++;
    /**
     * 绝技丹药<br />
     * 格式: a1-b1;a2-b2;a3-b3。<br />
     * 其中a1、a2等，代表丹药等级。b1、b2等，代表丹药数量。
     */
    @Type(String.class)
    public static final int MEDICINE_STUNT = END++;
    /**
     * 法术丹药<br />
     * 格式: a1-b1;a2-b2;a3-b3。<br />
     * 其中a1、a2等，代表丹药等级。b1、b2等，代表丹药数量。
     */
    @Type(String.class)
    public static final int MEDICINE_MAGIC = END++;

    /**
     * 属性个数
     */
    public static final int SIZE = END;
    /**
     * 属性类型
     */
    public static final RoleDef.PropertyType TYPE = RoleDef.PropertyType.ROLE_PROPS_STRING;

    public RoleStringProps() {
        super(SIZE, TYPE);
    }
}
