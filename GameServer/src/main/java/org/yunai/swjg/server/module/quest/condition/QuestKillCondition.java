package org.yunai.swjg.server.module.quest.condition;

import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.quest.QuestDef;
import org.yunai.swjg.server.rpc.struct.StQuestCondition;

/**
 * 任务条件：杀怪
 * User: yunai
 * Date: 13-5-9
 * Time: 上午1:20
 */
public class QuestKillCondition implements IQuestCondition {

    /**
     * 怪物编号
     */
    private Integer monsterId;
    /**
     * 杀怪物数量
     */
    private Integer count;

    public QuestKillCondition(Integer monsterId, Integer count) {
        this.monsterId = monsterId;
        this.count = count;
    }

    @Override
    public QuestDef.Condition getCondition() {
        return QuestDef.Condition.KILL;
    }

    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public Integer getSubject() {
        return monsterId;
    }

    @Override
    public boolean isMeet(Player player, Integer questId, boolean showError) {
        Integer doCount = player.getQuestDiary().getFinishedCount(questId, getCondition(), monsterId);
        return doCount >= count;
    }

    @Override
    public void onAccept(Player player) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onFinish(Player player) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * 由于刚接这个任务的时候，杀的怪肯定数量为0
     *
     * @param player 玩家信息
     * @return 0
     */
    @Override
    public int initParam(Player player) {
        return 0;
    }

    @Override
    public StQuestCondition genFinishCondition(Player player, Integer questId) {
        Integer doCount = player.getQuestDiary().getFinishedCount(questId, getCondition(), monsterId);
        return new StQuestCondition(getCondition().getValue(), monsterId, count, doCount);
    }
}
