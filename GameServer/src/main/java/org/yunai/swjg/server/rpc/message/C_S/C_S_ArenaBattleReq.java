package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21807】: 竞技场挑战请求
 */
public class C_S_ArenaBattleReq extends GameMessage {
    public static final short CODE = 21807;

    /**
     * 挑战的名次
     */
    private Integer rank;

    public C_S_ArenaBattleReq() {
    }

    public C_S_ArenaBattleReq(Integer rank) {
        this.rank = rank;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_ArenaBattleReq struct = new C_S_ArenaBattleReq();
            struct.setRank(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_ArenaBattleReq struct = (C_S_ArenaBattleReq) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byteArray.create();
            byteArray.putInt(struct.getRank());
            return byteArray;
        }
    }
}