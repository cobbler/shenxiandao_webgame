package org.yunai.swjg.server.module.item.operation.move;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.item.container.Bag;
import org.yunai.swjg.server.module.item.container.CommonBag;
import org.yunai.swjg.server.module.item.container.EquipmentBag;
import org.yunai.swjg.server.module.item.container.ShoulderBag;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;

/**
 * 装备背包 -> 仓库背包
 * 装备背包 -> 主背包
 * User: yunai
 * Date: 13-6-6
 * Time: 下午7:43
 */
@Component
public class MoveEquipmentBag2ShoulderBagOperation extends AbstractItemMoveOperation {

    @Override
    public boolean isSuitable(Player player, Item item, CommonBag toBag, int toIndex) {
        return item.getBagType() == Bag.BagType.EQUIPMENT
                && (toBag.getBagType() == Bag.BagType.DEPOT || toBag.getBagType() == Bag.BagType.PRIM);
    }

    @Override
    protected boolean canMoveImpl(Player player, Item item, CommonBag toBag, int toIndex) {
        return true;
    }

    @Override
    protected boolean moveImpl(Player player, Item item, CommonBag toBag, int toIndex) {
        EquipmentBag equipmentBag = (EquipmentBag) item.getBag();
        return equipmentBag.unEquipment(item, (ShoulderBag) toBag, toIndex);
    }
}