import org.yunai.swjg.server.module.scene.command.NormalSceneEnterCommand;

import java.lang.reflect.ParameterizedType;

/**
 * 命令测试
 * User: yunai
 * Date: 13-5-13
 * Time: 下午9:58
 */
public class CommandTest {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        System.out.println(NormalSceneEnterCommand.class.getGenericSuperclass().getClass());
//        System.out.println(EnterSceneCommand.class.getGenericSuperclass().);
        ParameterizedType pt = (ParameterizedType) NormalSceneEnterCommand.class.getGenericSuperclass();
        System.out.println(pt.getActualTypeArguments()[0]);
        Class type = (Class) pt.getActualTypeArguments()[0];
        System.out.println(type.getField("CODE").get(type));

        System.out.println(new NormalSceneEnterCommand().getCode());
    }


}
