package org.yunai.swjg.server.module.quest.event;

import org.yunai.swjg.server.module.item.event.ItemCountChangeEvent;
import org.yunai.yfserver.event.EventDispatcher;

/**
 * {@link QuestItemCountChangeEventListener}测试类
 * User: yunai
 * Date: 13-5-11
 * Time: 上午1:31
 */
public class QuestItemCountChangeEventListenerTest {

    public static void main(String[] args) {
        EventDispatcher dispatcher = new EventDispatcher();
        dispatcher.register(ItemCountChangeEvent.class, new QuestItemCountChangeEventListener());
//        dispatcher.fireEvent(new ItemCountChangeEvent());
    }
}
