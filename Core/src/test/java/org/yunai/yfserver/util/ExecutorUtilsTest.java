package org.yunai.yfserver.util;

import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * {@link java.util.concurrent.ExecutorService}的工具集测试类
 * User: yunai
 * Date: 13-4-28
 * Time: 下午12:17
 */
public class ExecutorUtilsTest {

    @Test
    public void testShutdownAndAwaitTermination1() {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(10);
        pool.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("[线程A]执行下逻辑开始");
                System.out.println("[线程B]执行下逻辑结束");
            }
        }, 1, TimeUnit.MINUTES);
        ExecutorUtils.shutdownAndAwaitTermination(pool);
    }

    @Test
    public void testShutdownAndAwaitTermination2() {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(10);
        pool.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("[线程A]执行下逻辑开始");
                try {
                    Thread.sleep(1000 * 60 * 2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("[线程B]执行下逻辑结束");
            }
        }, 1, TimeUnit.MINUTES);
        ExecutorUtils.shutdownAndAwaitTermination(pool);
    }
}
