package org.yunai.yfserver.schedule;

import org.yunai.yfserver.message.schedule.ScheduledCronMessage;

/**
 * 定时任务管理器接口
 * User: yunai
 * Date: 13-5-24
 * Time: 下午4:11
 */
public interface ScheduleService {

    /**
     * 根据cron字符串建立定时消息任务
     *
     * @param msg cron表达式定时消息
     */
    void schedule(ScheduledCronMessage msg);
}