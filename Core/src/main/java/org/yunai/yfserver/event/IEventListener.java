package org.yunai.yfserver.event;

/**
 * 事件监听者接口
 * User: yunai
 * Date: 13-5-11
 * Time: 上午12:44
 */
public interface IEventListener<E extends IEvent> {

    /**
     * 触发事件
     * @param event 事件
     */
    void fireEvent(E event);
}
