package org.yunai.yfserver.util;

import java.util.Random;

/**
 * 数学工具类
 * User: yunai
 * Date: 13-5-17
 * Time: 下午10:28
 */
public class MathUtils {

    private static final Random random = new Random();

    private MathUtils() {
    }

    public static boolean nextBoolean() {
        return random.nextBoolean();
    }

    public static int ceil2Int(Double val) {
        return (int) Math.ceil(val);
    }

    public static long ceil2Long(double val) {
        return (long) Math.ceil(val);
    }

    /**
     * 计算所需位置<br />
     * 即result = val % divideVal != 0 ? val / divideVal + 1 : val / divideVal;
     *
     * @param val       数量。当数量小于等于0时，返回0
     * @param divideVal 每个位置最大能装数量
     * @return 计算所需位置
     */
    public static long ceilX(long val, long divideVal) {
        return val <= 0 ? 0 : (val - 1 + divideVal) / divideVal;
    }

    /**
     * 计算所需位置<br />
     * 即result = val % divideVal != 0 ? val / divideVal + 1 : val / divideVal;
     *
     * @param val       数量。当数量小于等于0时，返回0
     * @param divideVal 每个位置最大能装数量
     * @return 计算所需位置
     */
    public static int ceilX(int val, int divideVal) {
        return val <= 0 ? 0 : (val - 1 + divideVal) / divideVal;
    }
}
