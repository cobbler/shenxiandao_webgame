package org.yunai.yfserver.common;

/**
 * 日志工厂
 * User: yunai
 * Date: 13-4-21
 * Time: 下午12:28
 */
public class LoggerFactory {

    public static enum Logger {
        /**
         * Server相关的日志
         */
        server("war.server"),
        /**
         * Game Server相关的日志
         */
        game("war.game"),
        /**
         * 登录相关的日志
         */
        login("war.login"),
        /**
         * 玩家相关的日志
         */
        player("war.player"),
        /**
         * 数据库相关的日志
         */
        db("war.db"),
        /**
         * 任务调度器相关的日志
         */
        schedule("war.schedule"),
        /**
         * 异步调度相关的日志
         */
        async("war.async"),
        /**
         * 消息相关日志
         */
        msg("war.msg"),
        /**
         *道具相关日志
         */
        item("war.item"),
        /**
         * 数据更新器日志
         */
        updater("war.updater"),
        /**
         * 场景相关日志
         */
        scene("war.scene"),
        /**
         * 副本相关日志
         */
        rep("war.rep"),
        /**
         * 怪物相关日志
         */
        monster("war.monster"),
        /**
         * 活动相关日志
         */
        activity("war.activity"),
        /**
         * 竞技场相关日志
         */
        arena("war.arena"),
        /**
         * 伙伴相关日志
         */
        partner("war.partner");

        /**
         * 模块名
         */
        private String name;

        private Logger(String name) {
            this.name = name;
        }
    }

    public static org.slf4j.Logger getLogger(Logger logger, Class<?> clazz) {
        return org.slf4j.LoggerFactory.getLogger(logger.name + " - " + clazz.getName());
    }

}
